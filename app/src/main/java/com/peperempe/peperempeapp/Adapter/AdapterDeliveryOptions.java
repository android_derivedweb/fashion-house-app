package com.peperempe.peperempeapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peperempe.peperempeapp.R;


public class AdapterDeliveryOptions extends RecyclerView.Adapter<AdapterDeliveryOptions.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;


    public AdapterDeliveryOptions(Context mContext, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_delivery_options, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class Viewholder extends RecyclerView.ViewHolder {




        public Viewholder(@NonNull View itemView) {
            super(itemView);



        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, int item);
    }
}