package com.peperempe.peperempeapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.peperempe.peperempeapp.Model.ProductListModel;
import com.peperempe.peperempeapp.R;

import java.util.ArrayList;


public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<ProductListModel> productListModelArrayList;


    public AdapterProduct(Context mContext, ArrayList<ProductListModel> productListModelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.productListModelArrayList = productListModelArrayList;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_products, parent, false);
        return new Viewholder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        Glide.with(mContext).load(productListModelArrayList.get(position).getProductImage()).into(holder.image);

        holder.productTitle.setText(productListModelArrayList.get(position).getProductTitle());
        holder.productPrice.setText(mContext.getResources().getString(R.string.currency_symbol) + productListModelArrayList.get(position).getPrice());


        if (productListModelArrayList.get(position).isWishlist()){
            holder.addWishlist.setBackgroundResource(R.drawable.heart_fill);
        } else {
            holder.addWishlist.setBackgroundResource(R.drawable.heart_blank);
        }


        holder.addWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (productListModelArrayList.get(position).isWishlist()){
                    holder.addWishlist.setBackgroundResource(R.drawable.heart_blank);
                    productListModelArrayList.get(position).setWishlist(false);
                } else {
                    holder.addWishlist.setBackgroundResource(R.drawable.heart_fill);
                    productListModelArrayList.get(position).setWishlist(true);
                }

                listener.onItemClickWishList(position);

            }
        });


    }

    @Override
    public int getItemCount() {
        return productListModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView image, addWishlist;
        TextView productTitle, productPrice;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            productTitle = itemView.findViewById(R.id.productTitle);
            productPrice = itemView.findViewById(R.id.productPrice);
            addWishlist = itemView.findViewById(R.id.addWishlist);

        }
    }


    public interface OnItemClickListener {
        void onItemClick(int item);
        void onItemClickWishList(int item);
    }


}