package com.peperempe.peperempeapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peperempe.peperempeapp.Model.InterestedModel;
import com.peperempe.peperempeapp.R;

import java.util.ArrayList;


public class AdapterInterestedIn extends RecyclerView.Adapter<AdapterInterestedIn.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<InterestedModel> modelArrayList;


    public AdapterInterestedIn(Context mContext, ArrayList<InterestedModel> modelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.modelArrayList = modelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_interested_in, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.clothing.setText(modelArrayList.get(position).getCategoryName());


    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView clothing;

        public Viewholder(@NonNull View itemView) {
            super(itemView);


            clothing = itemView.findViewById(R.id.clothing);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}