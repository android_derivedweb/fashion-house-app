package com.peperempe.peperempeapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.peperempe.peperempeapp.Model.ProductListModel;
import com.peperempe.peperempeapp.R;

import java.util.ArrayList;

public class ViewPagerWeek extends PagerAdapter {

    private Context mContext;
    private ArrayList<ProductListModel> productListModelArrayListRecommended;
    private final OnItemClickListener listener;

    public ViewPagerWeek(Context context, ArrayList<ProductListModel> productListModelArrayListRecommended, OnItemClickListener listener) {
        this.mContext = context;
        this.productListModelArrayListRecommended = productListModelArrayListRecommended;
        this.listener = listener;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.view_pager_week, container, false);


        container.addView(layout);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        ImageView image = container.findViewById(R.id.image);

        Glide.with(mContext).load(productListModelArrayListRecommended.get(position).getProductImage()).into(image);


        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return productListModelArrayListRecommended.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}
