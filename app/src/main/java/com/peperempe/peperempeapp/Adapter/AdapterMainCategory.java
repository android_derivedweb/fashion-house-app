package com.peperempe.peperempeapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peperempe.peperempeapp.Model.InterestedModel;
import com.peperempe.peperempeapp.R;

import java.util.ArrayList;


public class AdapterMainCategory extends RecyclerView.Adapter<AdapterMainCategory.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<InterestedModel> modelArrayList;


    public AdapterMainCategory(Context mContext, ArrayList<InterestedModel> modelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.modelArrayList = modelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_main_category, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.categoryName.setText(modelArrayList.get(position).getCategoryName());


    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView categoryName;

        public Viewholder(@NonNull View itemView) {
            super(itemView);


            categoryName = itemView.findViewById(R.id.categoryName);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}