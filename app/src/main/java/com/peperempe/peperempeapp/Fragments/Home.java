package com.peperempe.peperempeapp.Fragments;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.peperempe.peperempeapp.Activity.ItemsDetail;
import com.peperempe.peperempeapp.Adapter.AdapterHorizontalHome;
import com.peperempe.peperempeapp.Adapter.AdapterNewIn;
import com.peperempe.peperempeapp.Adapter.AdapterRecommended;
import com.peperempe.peperempeapp.Adapter.ViewPagerRecommended;
import com.peperempe.peperempeapp.Adapter.ViewPagerWeek;
import com.peperempe.peperempeapp.Model.ProductListModel;
import com.peperempe.peperempeapp.Model.SizeColorModel;
import com.peperempe.peperempeapp.R;
import com.peperempe.peperempeapp.Utils.UserSession;
import com.peperempe.peperempeapp.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Home extends Fragment {

    private DotsIndicator indicator, worm_dots_indicator2;
    private ViewPagerRecommended viewPagerRecommended;
    private ViewPagerWeek viewPagerWeek;
    private ArrayList<SizeColorModel> mDatasetServices = new ArrayList<>();

    private RecyclerView recNewIn;
    private AdapterRecommended adapterRecommended;
    private AdapterHorizontalHome adapterHorizontalHome;
    private AdapterNewIn adapterNewIn;

    private RecyclerView recHome;


    private UserSession session;
    private RequestQueue requestQueue;

    private TextView bannerText, DiscountOffTitle, DiscountOffSubTitle;
    private ImageView imageHome, image1;


    private ArrayList<ProductListModel> productListModelArrayList = new ArrayList<>();
    private ArrayList<ProductListModel> productListModelArrayListTrending = new ArrayList<>();
    private ArrayList<ProductListModel> productListModelArrayListRecommended = new ArrayList<>();
    private ArrayList<ProductListModel> productListModelArrayListRecent = new ArrayList<>();


    private String productIdOneItem = "";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_home, container, false);



        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue


        bannerText = view.findViewById(R.id.bannerText);
        DiscountOffTitle = view.findViewById(R.id.DiscountOffTitle);
        DiscountOffSubTitle = view.findViewById(R.id.DiscountOffSubTitle);
        imageHome = view.findViewById(R.id.imageHome);
        image1 = view.findViewById(R.id.image1);




        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        indicator = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator);
        viewPagerRecommended = new ViewPagerRecommended(getContext(), productListModelArrayList, new ViewPagerRecommended.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayList.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);


             /*   startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayList.get(item).getProductID()));
*/

            }
        });
        mViewPager.setAdapter(viewPagerRecommended);
        indicator.setViewPager(mViewPager);





        recHome = view.findViewById(R.id.recHome);
        recHome.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapterNewIn = new AdapterNewIn(getContext(), productListModelArrayListTrending, new AdapterNewIn.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayListTrending.get(item).getProductID()));

            }
        });
        recHome.setAdapter(adapterNewIn);





        // for viewpager 2
        ViewPager viewPager2 = (ViewPager) view.findViewById(R.id.viewPager2);
        worm_dots_indicator2 = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator2);
        viewPagerWeek = new ViewPagerWeek(getContext(), productListModelArrayListRecommended, new ViewPagerWeek.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayListRecommended.get(item).getProductID()));


            }
        });
        viewPager2.setAdapter(viewPagerWeek);
        worm_dots_indicator2.setViewPager(viewPager2);
        viewPagerWeek.notifyDataSetChanged();






        // for new items recycler
        recNewIn = view.findViewById(R.id.recNewIn);
        recNewIn.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapterHorizontalHome = new AdapterHorizontalHome(getContext(), productListModelArrayListRecent, new AdapterHorizontalHome.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayListRecent.get(item).getProductID()));
            }
        });
        recNewIn.setAdapter(adapterHorizontalHome);


        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productIdOneItem));
            }
        });



        getData();


        return view;

    }


    private void getData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "home?Token=" + session.getFirbaseDeviceToken(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseHome", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");


                                bannerText.setText(object.getString("ShippingText"));
                                DiscountOffTitle.setText(object.getString("DiscountOffTitle"));
                                DiscountOffSubTitle.setText(object.getString("DiscountOffSubTitle"));
                                Glide.with(getContext()).load(object.getString("DiscountOffImage")).into(imageHome);


                                //
                                JSONArray jsonArray = object.getJSONArray("new_arrival");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object1 = jsonArray.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object1.getString("ProductID"));
                                    productListModel.setProductTitle(object1.getString("ProductTitle"));
                                    productListModel.setProductImage(object1.getString("ProductImage"));

                                    productListModelArrayList.add(productListModel);
                                }
                                viewPagerRecommended.notifyDataSetChanged();


                                //
                                JSONObject object1 = object.getJSONObject("product");

                                Glide.with(getContext()).load(object1.getString("ProductImage")).into(image1);
                                productIdOneItem = object1.getString("ProductID");




                                //
                                JSONArray jsonArray1 = object.getJSONArray("trending");

                                for (int i = 0; i < jsonArray1.length(); i++){
                                    JSONObject object2 = jsonArray1.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object2.getString("ProductID"));
                                    productListModel.setBrandName(object2.getString("BrandName"));
                                    productListModel.setProductTitle(object2.getString("ProductTitle"));
                                    productListModel.setProductImage(object2.getString("ProductImage"));

                                    productListModelArrayListTrending.add(productListModel);
                                }
                                adapterNewIn.notifyDataSetChanged();




                                //
                                JSONArray jsonArray2 = object.getJSONArray("recommended_for_you");

                                for (int i = 0; i < jsonArray2.length(); i++){
                                    JSONObject object2 = jsonArray2.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object2.getString("ProductID"));
                                    productListModel.setProductImage(object2.getString("ProductImage"));

                                    productListModelArrayListRecommended.add(productListModel);
                                }
                                viewPagerWeek.notifyDataSetChanged();






                                //
                                JSONArray jsonArray4 = object.getJSONArray("trending");

                                for (int i = 0; i < jsonArray4.length(); i++){
                                    JSONObject object2 = jsonArray4.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object2.getString("ProductID"));
                                    productListModel.setBrandName(object2.getString("BrandName"));
                                    productListModel.setProductTitle(object2.getString("ProductTitle"));
                                    productListModel.setProductImage(object2.getString("ProductImage"));

                                    productListModelArrayListRecent.add(productListModel);
                                }
                                adapterHorizontalHome.notifyDataSetChanged();



                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //   params.put("services[]", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }




    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }


}