package com.peperempe.peperempeapp.Fragments;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.peperempe.peperempeapp.Activity.ProductList;
import com.peperempe.peperempeapp.Adapter.AdapterSearchInner;
import com.peperempe.peperempeapp.Model.InterestedModel;
import com.peperempe.peperempeapp.R;
import com.peperempe.peperempeapp.Utils.UserSession;
import com.peperempe.peperempeapp.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchInner extends Fragment {

    private RecyclerView recSearchInner;
    private AdapterSearchInner adapterSearchInner;
    private String subId;
    private ArrayList<InterestedModel> interestedModelArrayList = new ArrayList<>();


    private UserSession session;
    private RequestQueue requestQueue;


    public SearchInner(String subId) {
        this.subId = subId;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_search_inner, container, false);


        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue



        recSearchInner = view.findViewById(R.id.recSearchInner);
        recSearchInner.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterSearchInner = new AdapterSearchInner(getContext(), interestedModelArrayList, new AdapterSearchInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), ProductList.class).putExtra("BrandID", interestedModelArrayList.get(item).getCategoryID()));

            }
        });
        recSearchInner.setAdapter(adapterSearchInner);


        getSubCategory(subId);


        return view;

    }

    private void getSubCategory(String subId) {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "get-brands?SubCategoryID=" + subId,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    InterestedModel interestedModel = new InterestedModel();
                                    interestedModel.setCategoryID(object.getString("BrandID"));
                                    interestedModel.setCategoryName(object.getString("BrandName"));

                                    interestedModelArrayList.add(interestedModel);
                                }
                                adapterSearchInner.notifyDataSetChanged();




                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //   params.put("services[]", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



}