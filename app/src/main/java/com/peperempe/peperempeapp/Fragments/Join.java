package com.peperempe.peperempeapp.Fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.peperempe.peperempeapp.Activity.MainActivity;
import com.peperempe.peperempeapp.Adapter.AdapterInterestedSignUp;
import com.peperempe.peperempeapp.Model.InterestedModel;
import com.peperempe.peperempeapp.R;
import com.peperempe.peperempeapp.Utils.UserSession;
import com.peperempe.peperempeapp.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Join extends Fragment {

    private static TextView date;
    private EditText email, firstName, lastName, password;

    private RecyclerView recInterestedSignUp;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private String interestedIn = "";

    private UserSession session;
    private RequestQueue requestQueue;

    private ArrayList<InterestedModel> modelArrayList = new ArrayList<InterestedModel>();

    private AdapterInterestedSignUp adapterInterestedSignUp;

    private String interestedId = "";

    private static int forDateOfBirth = 0;
    private ArrayList<String> IntreresredList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_join, container, false);

        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue



        date = view.findViewById(R.id.date);
        email = view.findViewById(R.id.email);
        firstName = view.findViewById(R.id.firstName);
        lastName = view.findViewById(R.id.lastName);
        password = view.findViewById(R.id.password);




        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });


        recInterestedSignUp = view.findViewById(R.id.recInterestedSignUp);
        recInterestedSignUp.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterInterestedSignUp = new AdapterInterestedSignUp(getContext(), modelArrayList, new AdapterInterestedSignUp.OnItemClickListener() {
            @Override
            public void onItemClick(int item, boolean isChecked) {


                if(isChecked){
                    if(interestedId.isEmpty()) {
                        interestedId =  modelArrayList.get(item).getCategoryID();
                    }else {
                        interestedId =  interestedId + ","+ modelArrayList.get(item).getCategoryID();
                    }
                }

            }

        });
        recInterestedSignUp.setAdapter(adapterInterestedSignUp);




        Log.e("deviceToken", session.getFirbaseDeviceToken() + "");


        view.findViewById(R.id.joinUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (firstName.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "enter your first name", Toast.LENGTH_SHORT).show();
                } else if (lastName.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "enter your last name", Toast.LENGTH_SHORT).show();
                } else if (password.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "enter your password", Toast.LENGTH_SHORT).show();
                } else if (forDateOfBirth == 0){
                    Toast.makeText(getContext(), "enter your date of birth", Toast.LENGTH_SHORT).show();
                } else if (email.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "enter your email", Toast.LENGTH_SHORT).show();
                } else if (!email.getText().toString().trim().matches(emailPattern)){
                    Toast.makeText(getContext(), "enter valid email", Toast.LENGTH_SHORT).show();
                } else if (interestedId.equals("")){
                    Toast.makeText(getContext(), "select your interest", Toast.LENGTH_SHORT).show();
                } else {
                    Registration();
                }
            }
        });


        getInterestedIn();

        return view;
    }


    private void Registration() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "register",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + " --");
                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                session.createRegistrationSession(
                                        jsonObject.getJSONObject("data").getString("UserID"),
                                        jsonObject.getJSONObject("data").getString("InterestedIn"),
                                        jsonObject.getJSONObject("data").getString("ProfilePic"),
                                        jsonObject.getJSONObject("data").getString("FirstName"),
                                        jsonObject.getJSONObject("data").getString("LastName"),
                                        jsonObject.getJSONObject("data").getString("Email"),
                                        jsonObject.getJSONObject("data").getString("MobileNo"),
                                        jsonObject.getJSONObject("data").getString("GoogleID"),
                                        jsonObject.getJSONObject("data").getString("FacebookID"),
                                        "",
                                        jsonObject.getJSONObject("data").getString("TwitterID"),
                                        jsonObject.getJSONObject("data").getString("Address"),
                                        jsonObject.getJSONObject("data").getString("City"),
                                        jsonObject.getJSONObject("data").getString("State"),
                                        jsonObject.getJSONObject("data").getString("Country"),
                                        jsonObject.getJSONObject("data").getString("Pincode"),
                                        jsonObject.getJSONObject("data").getString("DateOfBirth"),
                                        jsonObject.getJSONObject("data").getString("IsEnablePushNotification"),
                                        jsonObject.getJSONObject("data").getString("OTP"),
                                        jsonObject.getJSONObject("data").getString("stripe_customer_id"),
                                        jsonObject.getJSONObject("data").getString("DeviceToken"),
                                        jsonObject.getJSONObject("data").getString("DeviceType"),
                                        jsonObject.getJSONObject("data").getString("APIToken"));


                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                getActivity().startActivity(intent);
                                getActivity().finish();

                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("FirstName", firstName.getText().toString());
                params.put("LastName", lastName.getText().toString());
                params.put("Email", email.getText().toString());
                params.put("Password", password.getText().toString());
                params.put("ConfirmPassword", password.getText().toString());
                params.put("DeviceType", "android");
                params.put("DeviceToken", session.getFirbaseDeviceToken());
                params.put("DateOfBirth", date.getText().toString());
                params.put("InterestedIn", interestedId);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
              /*  long imagename = System.currentTimeMillis();
                params.put("profile_image", new DataPart(imagename + ".png", getFileDataFromDrawable(profile_bitmap)));*/

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


    private void getInterestedIn() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-categories",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    InterestedModel interestedModel = new InterestedModel();
                                    interestedModel.setCategoryID(object.getString("CategoryID"));
                                    interestedModel.setCategoryName(object.getString("CategoryName"));

                                    modelArrayList.add(interestedModel);
                                }
                                adapterInterestedSignUp.notifyDataSetChanged();




                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //   params.put("services[]", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }




    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
            return  dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(calendar.getTime());

            date.setText(dateString);
            forDateOfBirth = 1;
           /* if(!time.getText().toString().equals("   Select  Date:")){

                try {
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
                    Date date11=new SimpleDateFormat("dd-MM-yyyy").parse(time.getText().toString());
                    printDifference(date2,date11);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }*/
        }
    }



}