package com.peperempe.peperempeapp.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.peperempe.peperempeapp.Activity.ItemsDetail;
import com.peperempe.peperempeapp.Activity.MainActivity;
import com.peperempe.peperempeapp.Adapter.AdapterMightAlsoLike;
import com.peperempe.peperempeapp.Adapter.AdapterNewIn;
import com.peperempe.peperempeapp.Adapter.ViewPagerProduct;
import com.peperempe.peperempeapp.Model.CategoryDetailsModel;
import com.peperempe.peperempeapp.Model.ProductListModel;
import com.peperempe.peperempeapp.Model.SelectCitySpinner;
import com.peperempe.peperempeapp.Model.SizeColorModel;
import com.peperempe.peperempeapp.R;
import com.peperempe.peperempeapp.Utils.Database;
import com.peperempe.peperempeapp.Utils.UserSession;
import com.peperempe.peperempeapp.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ItemsDetails extends Fragment {



    private ArrayList<SizeColorModel> mDatasetServices = new ArrayList<>();
    private DotsIndicator worm_dots_indicator;
    private ViewPagerProduct viewPagerProduct;

    private ImageView aDown, aUp, bDown, bUp, cDown, cUp, dDown, dUp;

  /*  private boolean des = true;
    private boolean size = true;
    private boolean care = true;
    private boolean shipping = true;

    private LinearLayout layoutDes;*/


    private RecyclerView recMightAlsoLike, recBuyTheLook;
    private AdapterNewIn adapterNewIn;
    private AdapterMightAlsoLike adapterMightAlsoLike;


    private ImageView imageProduct, imageWishlist;
    private TextView description, deliveryInfo, title, price, addToBag;


    private UserSession session;
    private RequestQueue requestQueue;

    private int currentStatusWishlist = 0;


    private Database dbHelper;
    private ArrayList<CategoryDetailsModel> detailsModelArrayList = new ArrayList<>();

    private ArrayList<SizeColorModel> arraySize = new ArrayList<>();
    private ArrayList<SizeColorModel> arrayColor = new ArrayList<>();

    private String imageCart;
    private String titleCart;
    private String priceCart;
    private String availQuantity;
    private String color = "";
    private String size = "";
    private String colorId = "";
    private String sizeId = "";

    private int colorInt = 0;
    private int sizeInt = 0;

    private Spinner colorSpinner, sizeSpinner;

    private ArrayList<ProductListModel> productListModelArrayList = new ArrayList<>();


    private TextView noStock;
    private String ProductID = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_items_details, container, false);


        imageProduct = view.findViewById(R.id.imageProduct);
        description = view.findViewById(R.id.description);
        title = view.findViewById(R.id.title);
        price = view.findViewById(R.id.price);
        imageWishlist = view.findViewById(R.id.imageWishlist);
        addToBag = view.findViewById(R.id.addToBag);
        colorSpinner = view.findViewById(R.id.colorSpinner);
        sizeSpinner = view.findViewById(R.id.sizeSpinner);
        deliveryInfo = view.findViewById(R.id.deliveryInfo);
        noStock = view.findViewById(R.id.noStock);


        try {
            ProductID = getArguments().getString("ProductID");

        } catch (Exception e) {

        }


        dbHelper = new Database(getActivity());

        detailsModelArrayList = dbHelper.getAllUser();

        addToBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //         Log.e("cdfdefedf", dealOptionIdCart + "--");

                if (Integer.parseInt(availQuantity) != 0) {

                    if (!addToBag.getText().toString().equals("REMOVE FROM BAG")) {
                        if (colorInt == 0) {
                            Toast.makeText(getActivity(), "Select Color", Toast.LENGTH_SHORT).show();
                        } else if (sizeInt == 0) {
                            Toast.makeText(getActivity(), "Select Size", Toast.LENGTH_SHORT).show();
                        } else {
                            dbHelper.InsertDetails(imageCart, titleCart, ProductID, priceCart, color, colorId, size, sizeId, "1", availQuantity);
                            addToBag.setText("REMOVE FROM BAG");
                        }
                    } else {
                        dbHelper.removeCart(ProductID);
                        addToBag.setText("ADD TO BAG");
                    }
                } else {
                    Toast.makeText(getActivity(), "Item not available.", Toast.LENGTH_SHORT).show();
                }


                detailsModelArrayList = dbHelper.getAllUser();

                MainActivity.bagCount.setText(String.valueOf(detailsModelArrayList.size()));



            }
        });
        session = new UserSession(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue




        for (int t = 0; t < detailsModelArrayList.size(); t++){
            if (detailsModelArrayList.get(t).getDeal_id().equals(ProductID)){
                addToBag.setText("REMOVE FROM BAG");
            } else if (detailsModelArrayList.isEmpty()){

            }

            Log.e("sfsfsdf", detailsModelArrayList.get(t).getDeal_id() +"--" + ProductID);

        }





        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        worm_dots_indicator = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator);
        viewPagerProduct = new ViewPagerProduct(getActivity(), mDatasetServices, new ViewPagerProduct.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        mViewPager.setAdapter(viewPagerProduct);
        worm_dots_indicator.setViewPager(mViewPager);



        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();


                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                }
            }
        });



        colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != arrayColor.size() - 1) {
                    colorInt = 1;
                    color = arrayColor.get(position).getProductVariantOption();
                    colorId = arrayColor.get(position).getProductVariantOptionID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != arraySize.size() - 1) {
                    sizeInt = 1;
                    size = arraySize.get(position).getProductVariantOption();
                    sizeId = arraySize.get(position).getProductVariantOptionID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });







        view.findViewById(R.id.imageWishlist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (currentStatusWishlist == 0){
                    imageWishlist.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.heart_fill));
                    currentStatusWishlist = 1;
                } else if (currentStatusWishlist == 1){
                    imageWishlist.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.heart_blank));
                    currentStatusWishlist = 0;
                }

                setWishlist(ProductID);

            }
        });






        recMightAlsoLike = view.findViewById(R.id.recMightAlsoLike);
        recMightAlsoLike.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapterMightAlsoLike = new AdapterMightAlsoLike(getActivity(), productListModelArrayList, new AdapterMightAlsoLike.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getActivity(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayList.get(item).getProductID()));
            }

            @Override
            public void onItemClickWishList(int item) {
                setWishlist(productListModelArrayList.get(item).getProductID());

            }
        });
        recMightAlsoLike.setAdapter(adapterMightAlsoLike);




        recBuyTheLook = view.findViewById(R.id.recBuyTheLook);
        recBuyTheLook.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
     /*   adapterMightAlsoLike = new AdapterMightAlsoLike(getActivity(), new AdapterMightAlsoLike.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });*/
        recBuyTheLook.setAdapter(adapterMightAlsoLike);


     /*   view.findViewById(R.id.showAllText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ShowAllItems.class));
            }
        });*/


        /*
        aDown = view.findViewById(R.id.aDown);
        aUp = view.findViewById(R.id.aUp);
        bDown = view.findViewById(R.id.bDown);
        bUp = view.findViewById(R.id.bUp);
        cDown = view.findViewById(R.id.cDown);
        cUp = view.findViewById(R.id.cUp);
        dDown = view.findViewById(R.id.dDown);
        dUp = view.findViewById(R.id.dUp);

        layoutDes = view.findViewById(R.id.layoutDes);



        view.findViewById(R.id.relative1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (des){

                    aDown.setVisibility(View.GONE);
                    aUp.setVisibility(View.VISIBLE);
                    bDown.setVisibility(View.VISIBLE);
                    bUp.setVisibility(View.GONE);
                    cDown.setVisibility(View.VISIBLE);
                    cUp.setVisibility(View.GONE);
                    dDown.setVisibility(View.VISIBLE);
                    dUp.setVisibility(View.GONE);

                    layoutDes.setVisibility(View.VISIBLE);


                    des = false;
                    size = true;
                    care = true;
                    shipping = true;

                } else {
                    aDown.setVisibility(View.VISIBLE);
                    aUp.setVisibility(View.GONE);

                    layoutDes.setVisibility(View.GONE);


                    des = true;
                    size = true;
                    care = true;
                    shipping = true;
                }
            }
        });

        view.findViewById(R.id.relative2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (size){

                    aDown.setVisibility(View.VISIBLE);
                    aUp.setVisibility(View.GONE);
                    bDown.setVisibility(View.GONE);
                    bUp.setVisibility(View.VISIBLE);
                    cDown.setVisibility(View.VISIBLE);
                    cUp.setVisibility(View.GONE);
                    dDown.setVisibility(View.VISIBLE);
                    dUp.setVisibility(View.GONE);


                    des = true;
                    size = false;
                    care = true;
                    shipping = true;

                } else {
                    bDown.setVisibility(View.VISIBLE);
                    bUp.setVisibility(View.GONE);

                    des = true;
                    size = true;
                    care = true;
                    shipping = true;
                }
            }
        });

        view.findViewById(R.id.relative3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (care){

                    aDown.setVisibility(View.VISIBLE);
                    aUp.setVisibility(View.GONE);
                    bDown.setVisibility(View.VISIBLE);
                    bUp.setVisibility(View.GONE);
                    cDown.setVisibility(View.GONE);
                    cUp.setVisibility(View.VISIBLE);
                    dDown.setVisibility(View.VISIBLE);
                    dUp.setVisibility(View.GONE);


                    des = true;
                    size = true;
                    care = false;
                    shipping = true;

                } else {
                    cDown.setVisibility(View.VISIBLE);
                    cUp.setVisibility(View.GONE);

                    des = true;
                    size = true;
                    care = true;
                    shipping = true;
                }
            }
        });

        view.findViewById(R.id.relative4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shipping){

                    aDown.setVisibility(View.VISIBLE);
                    aUp.setVisibility(View.GONE);
                    bDown.setVisibility(View.VISIBLE);
                    bUp.setVisibility(View.GONE);
                    cDown.setVisibility(View.VISIBLE);
                    cUp.setVisibility(View.GONE);
                    dDown.setVisibility(View.GONE);
                    dUp.setVisibility(View.VISIBLE);


                    des = true;
                    size = true;
                    care = true;
                    shipping = false;

                } else {
                    dDown.setVisibility(View.VISIBLE);
                    dUp.setVisibility(View.GONE);

                    des = true;
                    size = true;
                    care = true;
                    shipping = true;
                }
            }
        });*/

        getDetails(ProductID);
        



        return view;

    }



    private void setWishlist(String productId) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                UserSession.BASEURL + "add-to-wishlist",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {



                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                            }else {
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductID", productId);
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }




    private void getDetails(String ProductID) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "product-details",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                JSONObject object = jsonObject.getJSONObject("data");

                                Glide.with(getActivity()).load(object.getString("ProductImage")).into(imageProduct);

                                title.setText(object.getString("ProductTitle"));
                                price.setText(getResources().getString(R.string.currency_symbol) + object.getString("Price"));
                                description.setText(object.getString("Description"));
                                deliveryInfo.setText(object.getString("DeliveryReturnInfo"));


                                if (object.getString("IsWishlist").equals("1")){
                                    imageWishlist.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.heart_fill));
                                    currentStatusWishlist = 1;
                                } else if (object.getString("IsWishlist").equals("0")){
                                    imageWishlist.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.heart_blank));
                                    currentStatusWishlist = 0;
                                }



                                imageCart = object.getString("ProductImage");
                                titleCart = object.getString("ProductTitle");
                                availQuantity = object.getString("Quantity");
                                priceCart = object.getString("Price");

                                if (Integer.parseInt(availQuantity) == 0){
                                    noStock.setVisibility(View.VISIBLE);
                                }


                                JSONArray jsonArray = object.getJSONArray("product_variants");

                                JSONObject objectSize = jsonArray.getJSONObject(0);

                                JSONArray jsonArray1 = objectSize.getJSONArray("options");

                                for (int i = 0; i < jsonArray1.length(); i++){
                                    JSONObject object1 = jsonArray1.getJSONObject(i);
                                    SizeColorModel sizeColorModel = new SizeColorModel();
                                    sizeColorModel.setProductVariantOptionID(object1.getString("ProductVariantOptionID"));
                                    sizeColorModel.setProductVariantOption(object1.getString("ProductVariantOption"));

                                    arraySize.add(sizeColorModel);
                                }
                                SizeColorModel sizeColorModel3 = new SizeColorModel();
                                sizeColorModel3.setProductVariantOptionID("");
                                sizeColorModel3.setProductVariantOption("Size");
                                arraySize.add(sizeColorModel3);


                                SelectCitySpinner adapter = new SelectCitySpinner(getActivity(),
                                        android.R.layout.simple_spinner_item,
                                        arraySize);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                                sizeSpinner.setAdapter(adapter);
                                sizeSpinner.setSelection(adapter.getCount());







                                JSONObject objectColor = jsonArray.getJSONObject(1);

                                JSONArray jsonArray2 = objectColor.getJSONArray("options");

                                for (int i = 0; i < jsonArray2.length(); i++){
                                    JSONObject object1 = jsonArray2.getJSONObject(i);
                                    SizeColorModel sizeColorModel = new SizeColorModel();
                                    sizeColorModel.setProductVariantOptionID(object1.getString("ProductVariantOptionID"));
                                    sizeColorModel.setProductVariantOption(object1.getString("ProductVariantOption"));

                                    arrayColor.add(sizeColorModel);
                                }
                                SizeColorModel sizeColorModel6 = new SizeColorModel();
                                sizeColorModel6.setProductVariantOptionID("");
                                sizeColorModel6.setProductVariantOption("Color");
                                arrayColor.add(sizeColorModel6);


                                SelectCitySpinner adapter1 = new SelectCitySpinner(getActivity(),
                                        android.R.layout.simple_spinner_item,
                                        arrayColor);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                                colorSpinner.setAdapter(adapter1);
                                colorSpinner.setSelection(adapter1.getCount());




                                JSONArray jsonArray3 = object.getJSONArray("you_might_also_like");

                                for (int i = 0; i < jsonArray3.length(); i++){

                                    JSONObject object1 = jsonArray3.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object1.getString("ProductID"));
                                    productListModel.setPrice(object1.getString("Price"));
                                    productListModel.setDescription(object1.getString("Description"));
                                    productListModel.setProductImage(object1.getString("ProductImage"));

                                    if (object1.getString("IsWishlist").equals("0")){
                                        productListModel.setWishlist(false);
                                    } else if (object1.getString("IsWishlist").equals("1")){
                                        productListModel.setWishlist(true);
                                    }

                                    productListModelArrayList.add(productListModel);
                                }

                                adapterMightAlsoLike.notifyDataSetChanged();




                                //      Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }else {
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductID", ProductID);
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }






}