package com.peperempe.peperempeapp.Fragments;

import static com.peperempe.peperempeapp.Activity.MainActivity.image1;
import static com.peperempe.peperempeapp.Activity.MainActivity.image2;
import static com.peperempe.peperempeapp.Activity.MainActivity.image3;
import static com.peperempe.peperempeapp.Activity.MainActivity.image4;
import static com.peperempe.peperempeapp.Activity.MainActivity.image5;
import static com.peperempe.peperempeapp.Activity.MainActivity.tagAppName;
import static com.peperempe.peperempeapp.Activity.MainActivity.text1;
import static com.peperempe.peperempeapp.Activity.MainActivity.text2;
import static com.peperempe.peperempeapp.Activity.MainActivity.text3;
import static com.peperempe.peperempeapp.Activity.MainActivity.text4;
import static com.peperempe.peperempeapp.Activity.MainActivity.text5;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.peperempe.peperempeapp.Activity.AboutPeperempe;
import com.peperempe.peperempeapp.Activity.AddressList;
import com.peperempe.peperempeapp.Activity.ChangePassword;
import com.peperempe.peperempeapp.Activity.OrderReturn;
import com.peperempe.peperempeapp.Activity.PersonalProfile;
import com.peperempe.peperempeapp.Activity.PrivacyPolicy;
import com.peperempe.peperempeapp.Activity.ReturnNow;
import com.peperempe.peperempeapp.Activity.ShippingNotice;
import com.peperempe.peperempeapp.Activity.TermsCondition;
import com.peperempe.peperempeapp.Activity.WelcomeScreen;
import com.peperempe.peperempeapp.Model.CategoryDetailsModel;
import com.peperempe.peperempeapp.R;
import com.peperempe.peperempeapp.Utils.Database;
import com.peperempe.peperempeapp.Utils.UserSession;

import java.util.ArrayList;

public class Profile extends Fragment {


    private UserSession session;
    private RequestQueue requestQueue;
    private TextView profile_name;
    private TextView sign_out;

    private RelativeLayout shippingLayout;
    private LinearLayout shippingInner;

    private boolean condition = true;

    private ImageView arrRight, arrBottom;


    private TextView bagCount;
    private Database database;
    private ArrayList<CategoryDetailsModel> categoryDetailsModels = new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_profile, container, false);


        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        database = new Database(getContext());

        categoryDetailsModels = database.getAllUser();




        shippingLayout = view.findViewById(R.id.shippingLayout);
        shippingInner = view.findViewById(R.id.shippingInner);
        arrRight = view.findViewById(R.id.arrRight);
        arrBottom = view.findViewById(R.id.arrBottom);
        bagCount = view.findViewById(R.id.bagCount);


        bagCount.setText(String.valueOf(categoryDetailsModels.size()));


        shippingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (condition) {
                    shippingInner.setVisibility(View.VISIBLE);
                    arrRight.setVisibility(View.GONE);
                    arrBottom.setVisibility(View.VISIBLE);
                    condition = false;
                } else {
                    shippingInner.setVisibility(View.GONE);
                    arrRight.setVisibility(View.VISIBLE);
                    arrBottom.setVisibility(View.GONE);
                    condition = true;
                }

            }
        });




        view.findViewById(R.id.addressBook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddressList.class));
            }
        });

        view.findViewById(R.id.howToReturn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), OrderReturn.class));
            }
        });

        view.findViewById(R.id.changePass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), ChangePassword.class));
            }
        });

        view.findViewById(R.id.personalInformation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), PersonalProfile.class));
            }
        });

       view.findViewById(R.id.termsAndCondition).setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               startActivity(new Intent(getContext(), TermsCondition.class));
           }
       });

        view.findViewById(R.id.aboutPeperempe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), AboutPeperempe.class));
            }
        });

        view.findViewById(R.id.privacyPolicy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), PrivacyPolicy.class));
            }
        });


        view.findViewById(R.id.shippingInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ShippingNotice.class));

            }
        });


        view.findViewById(R.id.returnNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ReturnNow.class));
            }
        });




        profile_name = view.findViewById(R.id.profile_name);
        sign_out = view.findViewById(R.id.sign_out);
        profile_name.setText(session.getKeyFirstName());
        sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logout();
                LoginManager.getInstance().logOut();
                Intent intent = new Intent(getActivity(), WelcomeScreen.class);
                getActivity().startActivity(intent);
                getActivity().finish();

            }
        });
        view.findViewById(R.id.cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShoppingBag shoppingBag = new ShoppingBag();
                replaceFragment(R.id.fragmentLinearHome, shoppingBag, "shipping_bag", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag_active);

                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.pink));

                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Shopping Bag");

            }
        });


        return view;

    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


}