package com.peperempe.peperempeapp.Model;

public class ProductListModel {

    private String ProductID;
    private String ProductTitle;
    private String ProductCode;
    private String BrandName;
    private String BrandID;
    private String Description;
    private String Price;
    private String Quantity;
    private String CategoryName;
    private String SubCategoryName;
    private String ProductImage;
    private String CategoryID;
    private String FavouriteProductID;
    private boolean IsWishlist;


    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public boolean isWishlist() {
        return IsWishlist;
    }

    public void setWishlist(boolean wishlist) {
        IsWishlist = wishlist;
    }


    public String getFavouriteProductID() {
        return FavouriteProductID;
    }


    public void setFavouriteProductID(String favouriteProductID) {
        FavouriteProductID = favouriteProductID;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getProductTitle() {
        return ProductTitle;
    }

    public void setProductTitle(String productTitle) {
        ProductTitle = productTitle;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getBrandID() {
        return BrandID;
    }

    public void setBrandID(String brandID) {
        BrandID = brandID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getSubCategoryName() {
        return SubCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        SubCategoryName = subCategoryName;
    }

    public String getProductImage() {
        return ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }
}
