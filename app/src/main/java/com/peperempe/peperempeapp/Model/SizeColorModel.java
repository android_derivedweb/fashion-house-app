package com.peperempe.peperempeapp.Model;

public class SizeColorModel {

    String ProductVariantOptionID;
    String ProductVariantOption;

    public String getProductVariantOptionID() {
        return ProductVariantOptionID;
    }

    public void setProductVariantOptionID(String productVariantOptionID) {
        ProductVariantOptionID = productVariantOptionID;
    }

    public String getProductVariantOption() {
        return ProductVariantOption;
    }

    public void setProductVariantOption(String productVariantOption) {
        ProductVariantOption = productVariantOption;
    }
}
