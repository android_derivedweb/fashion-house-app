package com.peperempe.peperempeapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.peperempe.peperempeapp.R;

public class TermsCondition extends AppCompatActivity {

    private TextView textView1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        textView1 = findViewById(R.id.textView1);


        textView1.setText(Html.fromHtml("<p><strong>PRICING AND PAYMENT</strong></p>\n" +
                "<p>The prices shown on Peperempe website &amp; Apps shall include any VAT (or similar sales tax) at the prevailing rate for which we are responsible as the seller. The amount paid by customers will be the aforesaid price (prices indicated on our website) plus any delivery charges. The delivery charges will depend on the delivery option that customers choose and will be shown during the checkout process. &nbsp;<br />All prices, charges and fees are indicated in GBP (&pound;) and charged as such. As a result, all customers outside the UK will be subjected to the exchange rate applied by the customer&rsquo;s card Company.<br />Please note that all order made for receipt outside of the UK may be liable for import duty or other taxes, fees and charges that may be applied by the customs or other authorities at the order recipient country. In such cases, the customers&rsquo; is liable to such duty/taxes/fees/charges. It is therefore strongly advised that customers check with their local customs and authorities if they are unsure about whether these duties, taxes and charges may apply to their order(s).</p>\n" +
                "<p>If you have paid on your debit, credit or charge card, we will only charge you once the goods have been dispatched to the delivery address. Confirmation of the dispatch will be email to you.<br />&nbsp;<br />For payment with <span style=\"text-decoration: underline; color: #0000ff;\">PayPal</span> please visit&nbsp;<span style=\"text-decoration: underline; color: #0000ff;\">www.paypal.com</span> for information on PayPal payments policy &amp; TC.</p>\n" +
                "<p><strong>CARD VALIDATION</strong><br />&nbsp;<br />All credit/debit cardholders are subject to validation checks and authorisation by the card issuer. If the issuer of your payment card refuses to, or does not for any reason, authorise payment then you will be notified of this immediately at the check-out stage.<br />We do not handle the payment process. <strong>Payment completely and solely processed by</strong> <span style=\"text-decoration: underline; color: #0000ff;\">www.paypal.com</span>. At the payment stage during checkout, you will be redirected to the www.paypal.com official page for payment. After your payment has been completed and approved by <span style=\"text-decoration: underline; color: #0000ff;\">www.paypal.com</span>, you will be redirected to our page for confirmation of order.</p>\n" +
                "<p><strong>PRODUCT INFORMATION</strong></p>\n" +
                "<p>We make every effort to display accurate colours of our products on the website. However, the colours you see on your monitor may vary from the actual colour due to your monitor display.<br />&nbsp;<br />All sizes and measurements are approximate, however we make every effort to ensure they are as accurate as possible. Please use our size chart for help and feel free to contact our <span style=\"text-decoration: underline; color: #0000ff;\">customer&rsquo;s support</span> for assistance with sizing.<br />&nbsp;<br />We will take all reasonable care to ensure that all details, descriptions and prices of products appearing on the website are correct at the time when the relevant information was entered onto the system. We reserve the right to refuse orders where product information has been mis-published, including prices and promotions.<br />&nbsp;</p>\n" +
                "<p><strong>AUTHENTICITY</strong></p>\n" +
                "<p>Peperempe carries merchandise that is authentic, first run, and purchased directly from our designers.</p>\n" +
                "<p><strong>PURCHASE OF PRODUCTS</strong></p>\n" +
                "<p>When you place an order on our website we shall email you an order confirmation email. Our acceptance of your order does not take place until despatch of the order, at which point the contract for the purchase of goods will be made.<br />&nbsp;<br />Once you have checked out and you have received your order confirmation email you will not be able to make any changes to your order so please make sure that everything is correct before placing your order.<br />&nbsp;<br />We reserve the right to refuse an order. Non-acceptance of an order may, for example, result from one of the following:<br />&nbsp;<br /> The product ordered being unavailable from stock<br />&nbsp;<br /> The identification of an error within the product information, including price or promotion<br />&nbsp;<br />* If we suspect any fraudulent activity<br />&nbsp;<br />If there are any problems with your order we shall contact you. We reserve the right to reject any offer to purchase by you at any time. We will take all necessary measures to keep the details of your order and payment secure. Prices are subject to change without notice. These changes will not affect orders that have already been despatched.<br />&nbsp;<br />Goods are subject to availability. As there is a delay between the time when the order is placed and the time when the order is accepted, the stock position relating to particular items may change. If an item you have ordered becomes out of stock before we accept the order we shall notify you as soon as possible and you will not be charged for the out of stock items. However, we make every effort to continually update our website from time to time.<br />Also, please note that some website promotions may not be available to customers in particular jurisdictions.</p>"));


    }
}