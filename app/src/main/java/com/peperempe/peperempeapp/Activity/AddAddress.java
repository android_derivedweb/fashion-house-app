package com.peperempe.peperempeapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.peperempe.peperempeapp.R;
import com.peperempe.peperempeapp.Utils.UserSession;
import com.peperempe.peperempeapp.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddAddress extends AppCompatActivity {


    private EditText firstName, lastName, mobileNumber, country, houseNumber, shippingAddress, city, postCode;


    private UserSession session;
    private RequestQueue requestQueue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(AddAddress.this);
        requestQueue = Volley.newRequestQueue(AddAddress.this);//Creating the RequestQueue



        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        mobileNumber = findViewById(R.id.mobileNumber);
        country = findViewById(R.id.country);
        houseNumber = findViewById(R.id.houseNumber);
        shippingAddress = findViewById(R.id.shippingAddress);
        city = findViewById(R.id.city);
        postCode = findViewById(R.id.postCode);



        findViewById(R.id.btnAddNewAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (firstName.getText().toString().isEmpty()){
                    Toast.makeText(AddAddress.this, "enter first name", Toast.LENGTH_SHORT).show();
                } else if (lastName.getText().toString().isEmpty()){
                    Toast.makeText(AddAddress.this, "enter last name", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.getText().toString().isEmpty()){
                    Toast.makeText(AddAddress.this, "enter mobile number", Toast.LENGTH_SHORT).show();
                } else if (country.getText().toString().isEmpty()){
                    Toast.makeText(AddAddress.this, "enter country", Toast.LENGTH_SHORT).show();
                } else if (houseNumber.getText().toString().isEmpty()){
                    Toast.makeText(AddAddress.this, "enter house / flat name", Toast.LENGTH_SHORT).show();
                } else if (shippingAddress.getText().toString().isEmpty()){
                    Toast.makeText(AddAddress.this, "enter street name", Toast.LENGTH_SHORT).show();
                } else if (city.getText().toString().isEmpty()){
                    Toast.makeText(AddAddress.this, "enter city", Toast.LENGTH_SHORT).show();
                } else if (postCode.getText().toString().isEmpty()){
                    Toast.makeText(AddAddress.this, "enter postcode", Toast.LENGTH_SHORT).show();
                } else {
                    AddAddress();
                }
            }
        });


    }


    private void AddAddress() {
        final KProgressHUD progressDialog = KProgressHUD.create(AddAddress.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "add-shipping-address",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                finish();
                                Toast.makeText(AddAddress.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }else {
                                Toast.makeText(AddAddress.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(AddAddress.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(AddAddress.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(AddAddress.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(AddAddress.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("FirstName", firstName.getText().toString());
                params.put("LastName", lastName.getText().toString());
                params.put("MobileNumber", mobileNumber.getText().toString());
                params.put("ShippingHouseNo", houseNumber.getText().toString());
                params.put("ShippingAddress", shippingAddress.getText().toString());
                params.put("ShippingCity", city.getText().toString());
                params.put("ShippingCountry", country.getText().toString());
                params.put("ShippingPinCode", postCode.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



}