package com.peperempe.peperempeapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.peperempe.peperempeapp.R;

public class AboutPeperempe extends AppCompatActivity {


    private TextView textView1;
    private TextView textView2;
    private TextView textView3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_peperempe);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);

        textView1.setText(Html.fromHtml("<p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>In the midst of thought is the UNIQUENESS.</strong><br /><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;In the fullest of sight is the BEAUTY. </strong><br /><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;In the shadow of credence, is the POWERS</strong></p>"));
        textView2.setText(Html.fromHtml("<p style=\"text-align: center;\"><strong>~Peperempe</strong> (<em>Reveal your inner POWER</em>) created 2015</p>"));
        textView3.setText(Html.fromHtml(
                "<p><strong>Responsibilities</strong></p>\n" +
                "<p>Peperempe is committed to a world where a future is guaranteed. This commitment is translated into how we deal with our customers, the community and environment.</p>\n" +
                "<p>We strongly believe that our existence and future cannot be separated from our customers, the society, and the environment and we love it this way.</p>\n" +
                "<p>To ensure we are here will be here in the future, this is what we do;</p>" +
                "</ol>\n" +
                "<p>1. Satisfy our customers through the products and services we provide including, the pricing.</p>" +
                "<p>2. Produce and market environmentally-friendly</p>" +
                "<p>3. Engage and support the community</p>\n" +
                "<p>With these, our future is guaranteed.</p>"));


    }


}