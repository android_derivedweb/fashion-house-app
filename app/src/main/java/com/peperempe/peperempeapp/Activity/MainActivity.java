package com.peperempe.peperempeapp.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.peperempe.peperempeapp.Fragments.Home;
import com.peperempe.peperempeapp.Fragments.Me;
import com.peperempe.peperempeapp.Fragments.Profile;
import com.peperempe.peperempeapp.Fragments.Search;
import com.peperempe.peperempeapp.Fragments.ShoppingBag;
import com.peperempe.peperempeapp.Fragments.Wishlist;
import com.peperempe.peperempeapp.Model.CategoryDetailsModel;
import com.peperempe.peperempeapp.R;
import com.peperempe.peperempeapp.Utils.Database;
import com.peperempe.peperempeapp.Utils.UserSession;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private LinearLayout navLinear1, navLinear2, navLinear3, navLinear4, navLinear5;
    public static ImageView image1, image2, image3, image4, image5;
    public static TextView text1, text2, text3, text4, text5;

    public static TextView tagAppName;

    private UserSession session;
    public static CallbackManager mCallbackManager;
    public static boolean IsFromShopingBag = false;

    public static TextView bagCount;
    private Database database;
    private ArrayList<CategoryDetailsModel> categoryDetailsModels = new ArrayList<>();



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        mCallbackManager = CallbackManager.Factory.create();
        session = new UserSession(MainActivity.this);

        database = new Database(MainActivity.this);

        categoryDetailsModels = database.getAllUser();

        Log.e("deviceToken", session.getFirbaseDeviceToken() + "-");


        bagCount = findViewById(R.id.bagCount);

        bagCount.setText(String.valueOf(categoryDetailsModels.size()));


        setFooterNavigation();

    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


    private void setFooterNavigation() {

        navLinear1 = findViewById(R.id.navLinear1);
        navLinear2 = findViewById(R.id.navLinear2);
        navLinear3 = findViewById(R.id.navLinear3);
        navLinear4 = findViewById(R.id.navLinear4);
        navLinear5 = findViewById(R.id.navLinear5);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        text5 = findViewById(R.id.text5);


        tagAppName = findViewById(R.id.tagAppName);


        Home home = new Home();
        replaceFragment(R.id.fragmentLinearHome, home, "home", null);


        navLinear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Home home = new Home();
                replaceFragment(R.id.fragmentLinearHome, home, "home", null);

                image1.setImageResource(R.drawable.home_active);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.pink));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Peperempe");

            }
        });

        navLinear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Search search = new Search();
                replaceFragment(R.id.fragmentLinearHome, search, "search", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search_active);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.pink));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Search");
            }
        });

        navLinear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Wishlist wishlist = new Wishlist();
                replaceFragment(R.id.fragmentLinearHome, wishlist, "wishlist", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist_active);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.pink));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Wishlist");
            }
        });

        navLinear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (session.isLoggedIn()) {
                    Profile profile = new Profile();
                    replaceFragment(R.id.fragmentLinearHome, profile, "profile", null);

                } else {
                    Me me = new Me("0");
                    replaceFragment(R.id.fragmentLinearHome, me, "me", null);
                }

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user_active);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.pink));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Peperempe");

            }
        });


        navLinear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShoppingBag shoppingBag = new ShoppingBag();
                replaceFragment(R.id.fragmentLinearHome, shoppingBag, "shipping_bag", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag_active);

                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.pink));

                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Shopping Bag");

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        categoryDetailsModels = database.getAllUser();
        bagCount.setText(String.valueOf(categoryDetailsModels.size()));

    }
}